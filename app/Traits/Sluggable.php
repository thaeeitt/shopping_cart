<?php
namespace App\Traits;
trait Sluggable
{
	public static function bootSluggable()
	{
        static::saving(function ($model)
        {
            $setting = $model->sluggable();
            if(array_key_exists('source',$setting) and array_key_exists('column_name',$setting)){
                $source = $setting['source'];
                $column_name = $setting['column_name'];
            }
            else{
                throw new SlugException();
            }
            $count = 0;
            
            $check_field = strtolower(trim($source));
            $slug = $model->generateSlug($source);            //get all from table using sluggable attr;
            $records = $model->all()->pluck($column_name);
            //convert all to lowercase and trim
            $records = $records->map(function ($record) {
                return strtolower(trim($record));
            });
            $records->filter(function($record) use ($check_field, &$count){
                if($record === $check_field){
                    $count = $count + 1;
                }
            });
            $found = true;
            do {
                if($count == 0){
                    $slug_field = $slug;
                }
                else{
                    $slug_field = $slug . '-' . $count;
                }

                $rec = $model->where('slug',$slug_field)->first();
                if(!$rec){
                    $found = false;
                }
                $count++;
            } while ($found == true);
            
            if($model->getOriginal() == Null){
                $model->slug = $slug_field;
            }
        });
	}

	abstract public function sluggable(): array;

	public function generateSlug($string)
	{
		return strtolower(preg_replace(
			['/[^\w\s]+/', '/\s+/'],
			['', '-'],
			$string
		));
	}
}
