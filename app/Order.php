<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    public $guarded = [];

    public function getRouteKeyName()
    {
        return 'order_no';
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function products()
    {
        return $this->belongsToMany('App\Product')->withTrashed()->withPivot('id', 'quantity', 'unit_price', 'total_price');
    }
}
