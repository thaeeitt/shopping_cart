<?php

namespace App\Repositories;

class BaseRepository {
    
    public $model;

    public function __construct($model)
    {
        $this->model = $model;
    }

    public function all()
    {
        return $this->model->latest()->paginate(10);
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function findBySlug($slug)
    {
        return $this->model->whereSlug($slug)->first();
    }

    public function latest()
    {
        return $this->model->latest()->first();
    }

    public function store($data)
    {
        return $this->model->create($data);
    }

    public function update($model, $validated_data)
    {
        return $model->update($validated_data);
    }

    public function delete($model)
    {
        return $model->delete();
    }
}