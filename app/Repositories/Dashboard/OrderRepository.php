<?php

namespace App\Repositories\Dashboard;

use App\Order;
use App\Repositories\BaseRepository;

class OrderRepository extends BaseRepository
{
    public $model;
    public function __construct(Order $order)
    {
        parent::__construct($order);
        $this->model = $order;
    }

    public function getOrders($status)
    {
        return $this->model->whereStatus($status)->with('products')->get();
    }
}
