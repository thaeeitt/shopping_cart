<?php

namespace App\Repositories\Dashboard;

use App\User;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Hash;

class UserRepository extends BaseRepository {
    
    public function __construct(User $user)
    {
        parent::__construct($user);
    }

    public function customers()
    {
        return $this->model->where('is_admin',0)->latest()->paginate(10);
    }

    public function store($data)
    {
        $data['password'] = Hash::make($data['password']);
        return $this->model->create($data);
    }

    public function update($user, $data) 
    {
        return $user->update($data);
    }
}