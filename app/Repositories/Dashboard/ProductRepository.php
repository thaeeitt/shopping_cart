<?php

namespace App\Repositories\Dashboard;

use App\Product;
use App\Repositories\BaseRepository;

class ProductRepository extends BaseRepository
{

    public function __construct(Product $product)
    {
        parent::__construct($product);
    }

    public function store($data)
    {
        return $this->model->create($data);
    }

    public function update($product, $data)
    {
        return $product->update($data);
    }

    public function delete($product)
    {
        return $product->delete();
    }
}
