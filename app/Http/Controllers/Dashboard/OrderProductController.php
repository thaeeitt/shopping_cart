<?php

namespace App\Http\Controllers\Dashboard;

use App\Order;
use Illuminate\Http\Request;
use App\Http\Requests\OrderRequest;
use App\Http\Controllers\Controller;
use App\OrderProduct;
use Illuminate\Support\Facades\Lang;
use App\Repositories\Dashboard\OrderRepository;

class OrderProductController extends Controller
{

    public function edit(Order $order)
    {
        return view('dashboard.order_product.edit', compact('order'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Order $order, $order_product_id, Request $request)
    {
        $quantity = (int)$request->get('quantity');
        
        $order_product = OrderProduct::where('id', $order_product_id)->first();
        if($order_product) {
            $substracted_subtotal = $order->subtotal - $order_product->total_price;

            $updated_o_p_total_price = $quantity * $order_product->unit_price;
    
            //updating order_product's qty and total_price
            $order_product->quantity = $quantity;
            $order_product->total_price = $updated_o_p_total_price;
            $order_product->save();
    
            //updating order's subtotal and total price
            $order->subtotal = $substracted_subtotal + $updated_o_p_total_price;
    
            //
            $order->total = $substracted_subtotal + $updated_o_p_total_price;
            $order->save();
            
            return redirect()->route('orders.products.edit', $order->order_no)->with('success', Lang::get('message.object.update', ['object' => 'Order Product']));
        } else {

        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order, $order_product_id)
    {
        $order_product = OrderProduct::where('id', $order_product_id)->first();
        if($order_product) {
            
            //updating order's subtotal and total price
            $order->subtotal -= $order_product->total_price;
            $order->total -= $order_product->total_price;
            $order->save();

            $order_product->delete();
            return redirect()->route('orders.products.edit', $order->order_no)->with('success', Lang::get('message.object.delete', ['object' => 'Order Product']));
        }
    }
}
