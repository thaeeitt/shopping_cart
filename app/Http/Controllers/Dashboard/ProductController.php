<?php

namespace App\Http\Controllers\Dashboard;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashobard\ProductRequest;
use Illuminate\Support\Facades\Lang;
use App\Repositories\Dashboard\ProductRepository;

class ProductController extends Controller
{
    public $product_repo;
    public function __construct(ProductRepository $product_repo)
    {
        $this->product_repo = $product_repo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = $this->product_repo->all();
        return view('dashboard.product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $validated_data = $request->validated();
        $this->product_repo->store($validated_data);
        return redirect()->route('products.index')->with('success', Lang::get('message.object.create', ['object' => 'Product']));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('dashboard.product.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('dashboard.product.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, Product $product)
    {
        $validated_data = $request->validated();
        $this->product_repo->update($product, $validated_data);
        return redirect()->route('products.index')->with('success', Lang::get('message.object.update', ['object' => 'Product']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $this->product_repo->delete($product);
        return redirect()->route('products.index')->with('success', Lang::get('message.object.delete', ['object' => 'Product']));
    }
}
