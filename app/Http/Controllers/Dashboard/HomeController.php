<?php

namespace App\Http\Controllers\Dashboard;

use App\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{

    public function index()
    {
        $count = Order::whereDate('created_at', '=', date('Y-m-d'))->count();
        return view('dashboard.home', compact('count'));
    }
}
