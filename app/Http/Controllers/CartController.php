<?php

namespace App\Http\Controllers;

use App\Product;
use Gloudemans\Shoppingcart\Facades\Cart;
use Symfony\Component\HttpFoundation\Request;
use App\Repositories\Dashboard\ProductRepository;

class CartController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public $product_repo;
    public function __construct(ProductRepository $product_repo)
    {
        $this->product_repo = $product_repo;
    }



    public function checkQty(Request $request)
    {
        $qty = 0;
        $id = $request->get('id');
        $product = Product::where('id', $id)->first();
        if($product) $qty = $product->quantity;
        return [
            'qty' => $qty
        ];
    }

    public function cart()
    {
        return view('front.cartdetails');
    }
}
