<?php

namespace App\Http\Controllers;

use App\Customer;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\UserStoreRequest;
use Illuminate\Support\Facades\Session;
use Symfony\Component\HttpFoundation\Request;

class AuthController extends Controller
{

    public function loginForm()
    {
        return view('front.login_form');
    }
    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|string|email|max:200',
            'password' => 'required'
           ]);

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            if(!Auth::user()->is_admin) {
                return [
                    'message' => 'Logged in successfully!',
                    'status' => 'success',
                    'data' => Auth::user()
                ];
            } else{
                $data = [
                    'message' => 'Invalid Credentials',
                    'status' => 'error',
                    'data' => null
                ];
                return response()->json($data, 422);
            }
            
        } else {
            $data = [
                'message' => 'Invalid Credentials',
                'status' => 'error',
                'data' => null
            ];
            return response()->json($data, 422);
        }
    }

    public function register(UserStoreRequest $request)
    {
        $validated_data = $request->validated();
        $validated_data['password'] = bcrypt($validated_data['password']);
        User::create($validated_data);
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'is_admin' => 0])) {
            return [
                'message' => 'Logged in successfully!',
                'status' => 'success',
                'data' => Auth::user()
            ];
        } else {
            $data = [
                'message' => 'Invalid Credentials',
                'status' => 'error',
                'data' => null
            ];
            return response()->json($data, 422);
        }
    }

    public function logout()
    {
        Auth::logout();
        return 'success';
    }
}
