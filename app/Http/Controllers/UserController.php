<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function profile()
    {
        $id = Auth::user()->id;
        $user = User::with(['orders' => function ($query) {
            $query->orderBy('created_at', 'desc');
        }, 'orders.products'])->where('id', $id)->first();
        // $user = User::where('id', $id)->where('is_admin', 0)->first();
        return view('front.profile', compact('user'));
    }
}
