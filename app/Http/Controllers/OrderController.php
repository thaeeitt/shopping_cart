<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\OrderRequest;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function checkout(OrderRequest $request)
    {
        $validated_data = $request->validated();
        if($validated_data['products']) {
            // return Auth::user();
            // create order
            $data = [
                'order_no' => 'FT-'.Carbon::now()->timestamp,
                'user_id' => Auth::user()->id,
                'phone' => $validated_data['phone'],
                'address' => $validated_data['address']
            ];
            $order = Order::create($data);
            $sub_total = 0;
            foreach($validated_data['products'] as $cart_product) {
                //create order product
                $product = Product::whereId($cart_product['id'])->first();
                if($product) {
                    $quantity = $cart_product['quantity'];
                    if($quantity <= $product->quantity) {
                        $unit_price = $product->price;
                        $total_price = $unit_price * $quantity;
                        $sub_total += $total_price;
                        DB::table('order_product')->insert([
                            'order_id' => $order->id,
                            'product_id' => $product->id,
                            'quantity' => $quantity,
                            'unit_price' => $unit_price,
                            'total_price' => $total_price,
                        ]);
                        $product->quantity -= $quantity;
                        $product->save();
                    }
                }
            }
            $order->total = $sub_total;
            $order->save();
            return 'success';
            // return redirect()->route('profile');
        }
    }
}
