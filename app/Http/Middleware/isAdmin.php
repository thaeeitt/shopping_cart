<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Illuminate\Support\Facades\Auth;

class isAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // User role
        $id = Auth::user()->id;
        $user = User::find($id);
        
        if($user->is_admin) {
            return $next($request);
        } else {
            return redirect('/login')->with('permission_deny', 'You are not allowed to Dashboard Access.');
        }
        
    }
}
