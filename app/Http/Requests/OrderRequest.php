<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        return [
            'phone' => ['required', 'integer', 'digits_between:8,10'],
            'address' => ['required', 'string', 'max:255'],
            'card_no' => ['required', 'integer', 'digits: 10'],
            'card_name' => ['required', 'string', 'max:255'],
            'card_yy' => ['required', 'integer', 'digits:2'],
            'card_mm' => ['required', 'integer', 'digits:2'],
            'card_cvv' => ['required', 'integer', 'digits_between:3,4'],
            'products' => ['required']
        ];
    }
}
