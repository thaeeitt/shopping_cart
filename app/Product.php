<?php

namespace App;

use App\Traits\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use Sluggable, SoftDeletes;

    protected $fillable = ['name', 'slug', 'price', 'description', 'quantity'];

    public function sluggable()
    {
        return [
            'source' => $this->name,
            'column_name' => 'name',
        ];
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function orders()
    {
        return $this->belongsToMany('App\Order')->withPivot('quantity', 'unit_price', 'total_price');
    }
}
