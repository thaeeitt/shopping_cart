<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user1 = new User();
        $user1->name = 'Admin';
        $user1->email = 'thaeeithantun@gmail.com';
        $user1->password = bcrypt('password');
        $user1->is_admin = 1;
        $user1->save();
    }
}
