<?php

use App\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = ['Ring', 'Necklace', 'Earrings'];
        $slugs = ['ring', 'necklace', 'earrings'];
        $descriptions = ['Ruby Ring', 'Boyfriend Necklace', 'Bar Earrings'];
        $prices = [100000, 120000, 110000];
        foreach($products as $key=>$product)
        {
            Product::create([
                'name' => $product,
                'slug' => $slugs[$key],
                'price' => $prices[$key],
                'description' => $descriptions[$key],
                'quantity' => 50
            ]);
        }
        
    }
}
