<?php

use App\Product;
use Gloudemans\Shoppingcart\Facades\Cart;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes(['register' => false]);

Route::get('/', 'HomeController@index')->name('home');

Route::get('/products', 'ProductController@index')->name('products');
Route::get('/products/{product}', 'ProductController@details')->name('product');
Route::get('/cart', 'CartController@cart')->name('cart');
Route::get('/profile', 'UserController@profile')->name('profile')->middleware(['auth', 'isUser']);


// Auth routes
Route::post('/user_login', 'AuthController@login')->name('user-login');
Route::post('/user_register', 'AuthController@register')->name('user-register');
Route::post('/user_logout', 'AuthController@logout')->name('user-logout');

Route::get('/check_qty', 'CartController@checkQty')->name('check-qty');
Route::get('/products', 'ProductController@index')->name('products');
Route::get('/products/{product}', 'ProductController@details')->name('product');
Route::post('/checkout', 'OrderController@checkout')->name('checkout');