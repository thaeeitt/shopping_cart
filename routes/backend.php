<?php

Route::group(['middleware' => ['auth', 'isAdmin'], 'namespace' => 'Dashboard'], function () {
    Route::get('/', 'HomeController@index')->name('dashboard');
    Route::resource('/users', 'UserController');
    Route::resource('/products', 'ProductController');
    Route::resource('/orders', 'OrderController');
});
