/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */
 require('./bootstrap');
 
 window.Vue = require('vue');
 
 window.bus = new Vue();

import Vue from 'vue';
import VueSimpleAlert from "vue-simple-alert";

Vue.use(VueSimpleAlert);
 
 /**
  * Next, we will create a fresh Vue application instance and attach it to
  * the page. Then, you may begin adding components to this application
  * or customize the JavaScript scaffolding to fit your unique needs.
  */
 
 Vue.component('example', require('./components/Example.vue').default);
 Vue.component('product', require('./components/Product.vue').default);
 Vue.component('cart-count', require('./components/CartCount.vue').default);
 Vue.component('cart-detail', require('./components/CartDetail.vue').default);
 Vue.component('checkout-payment', require('./components/CheckoutPayment.vue').default);
 Vue.component('auth-form', require('./components/AuthForm.vue').default);
 Vue.component('nav-bar', require('./components/NavBar.vue').default);
 Vue.component('validation-errors', require('./components/ValidationError.vue').default);
  
 const app = new Vue({
     el: '#app',
     data: {
         cart: [],
         user: null
     },
     created(){
         this.getCart();
         this.getUser();
 
         bus.$on('added-to-cart', (product) => {
            this.addToCart(product)
         });
 
         bus.$on('remove-from-cart', (product) => {
             this.removeFromCart(product);
         });

        bus.$on('set-user', (user) => {
            this.setUser(user)
        });
 
     },
     computed: {
         cartTotal(){
             return this.cart.reduce((total, product) => {
                 return total + product.quantity * product.price;
             }, 0);
         },
         totalItems(){
             return this.cart.reduce((total, product) => {
                 return total + product.quantity;
             }, 0);
         },
         authenticated() {
             console.log(this.user)
            if(this.user) return true;
            else return false;
         }
     },
     methods: {
         logoutUser(){
            axios.post('/user_logout')
            .then((response) => {
                console.log(response.data);
                localStorage.removeItem('user')
            })
            .catch((error) => {
                console.error(error);
            });
         },
         setUser(user) {
            console.log('set')
            console.log(user)
            this.user = user
            localStorage.setItem('user', JSON.stringify(user));
         },
         getUser() {
             console.log('get user')
            if (localStorage && localStorage.getItem('user')) {
                this.user = JSON.parse(localStorage.getItem('user'));
            } 
         },
         getCart () {
             console.log('get cart')
             if (localStorage && localStorage.getItem('cart')) {
                 this.cart = JSON.parse(localStorage.getItem('cart'));
                 
 
             } else {
                 this.cart = [];
             }
         },
         addToCart(product){
            const matchingProductIndex = this.cart.findIndex((item) => {
                return item.id === product.id;
            });
            console.log(product.slug)
            var p_qty = 0;
            axios.get('/check_qty', {
                params: {
                    id: product.id
                }
            }).then((response) => {
                    console.log(response.data);
                    p_qty = response.data.qty;
                    if(p_qty) {
                        if (matchingProductIndex > -1) {
                            var qty = this.cart[matchingProductIndex].quantity + 1;
                            if(p_qty >= qty) {
                                this.cart[matchingProductIndex].quantity = qty;
                            } else {

                            }
                            
                        } else {
                            product.quantity = 1;
                            this.cart.push(product);
                        }
                        localStorage.setItem('cart', JSON.stringify(this.cart));
                    }
                })
                .catch((error) => {
                    console.error(error);
                });
            
         },
 
         removeFromCart(product){
             const matchingProductIndex = this.cart.findIndex((item) => {
                 return item.id == product.id;
             });
 
             if (this.cart[matchingProductIndex].quantity <= 1) {
                 this.cart.splice(matchingProductIndex, 1);
             } else {
                 this.cart[matchingProductIndex].quantity--;
             }
 
             localStorage.setItem('cart', JSON.stringify(this.cart));
         }
 
     }
 });
 