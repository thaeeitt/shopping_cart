@extends('adminlte::page')

@section('css')
@stop

@section('content_header')

@stop

@section('content')

<div class="row pb-4">
  <div class="col-12 offset-sm-2 col-sm-8 offset-lg-3 col-lg-6">
    <div class="card card-dark">
      <div class="card-header">
        <h3 class="card-title font-weight-light">Add New Customer</h3>
      </div>
      
      <form role="form" method="POST" action="{{route('users.store')}}">
        @csrf
        <div class="card-body pb-1">
          <div class="form-group">
            <label class="font-weight-light">Customer Name </label><span class="text-danger ml-1">*</span>
            <input type="text" class="form-control" id="name" name="name" placeholder="Enter User Name" value="{{old('name')}}">
            @if ($errors->has('name'))
              <span class="help-block text-danger">
                <strong>{{ $errors->first('name') }}</strong>
              </span>
            @endif
          </div>

          <div class="form-group">
            <label class="font-weight-light">Email</label><span class="text-danger ml-1">*</span>
            <input type="email" class="form-control" id="email" name="email" placeholder="Enter User email" value="{{old('email')}}">
            @if ($errors->has('email'))
              <span class="help-block text-danger">
                <strong>{{ $errors->first('email') }}</strong>
              </span>
            @endif
          </div>

          <div class="form-group">
            <label class="font-weight-light">Password</label><span class="text-danger ml-1">*</span>
            <input type="password" class="form-control" id="password" name="password" placeholder="Enter User password" value="{{old('password')}}">
            @if ($errors->has('password'))
              <span class="help-block text-danger">
                <strong>{{ $errors->first('password') }}</strong>
              </span>
            @endif
          </div>

          <div class="form-group">
            <label class="font-weight-light">Password Confirmation</label><span class="text-danger ml-1">*</span>
            <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Enter User Name" value="{{old('password_confirmation')}}">
            @if ($errors->has('password_confirmation'))
              <span class="help-block text-danger">
                <strong>{{ $errors->first('password_confirmation') }}</strong>
              </span>
            @endif
          </div>

          <!-- /.card-body -->

          <div class="form-group mt-5 text-center">
            <button type="submit" class="btn btn-dark mr-2">Create User</button>
            <a href="{{route('users.index')}}" class="btn btn-outline-dark">Cancel</a>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

@stop

@section('js')

@endsection