@extends('adminlte::page')

@section('css')
@stop

@section('content_header')

@stop

@section('content')

<div class="row pb-4">
    <div class="col-12 offset-sm-2 col-sm-8 offset-lg-3 col-lg-6">
    <div class="card card-dark">
        <div class="card-header">
        <h3 class="card-title font-weight-light">Edit Customer</h3>
        </div>
        
        <form role="form" method="POST" action="{{route('users.update', $user->id)}}">
            @method('put')
            @csrf
            <div class="card-body pb-1">
                <div class="form-group">
                    <label class="font-weight-light">User email </label><span class="text-danger ml-1">*</span>
                    <input type="text" class="form-control" id="name" name="name" value="{{$user->name}}">
                    @if ($errors->has('name'))
                    <span class="help-block text-danger">
                    <strong>{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group">
                    <label class="font-weight-light">Email </label><span class="text-danger ml-1">*</span>
                    <input type="email" class="form-control" id="email" name="email" value="{{$user->email}}">
                    @if ($errors->has('email'))
                    <span class="help-block text-danger">
                    <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group mt-5 text-center">
                    <button type="submit" class="btn btn-dark mr-2">Update user</button>
                    <a href="{{route('users.index')}}" class="btn btn-outline-dark">Cancel</a>
                </div>
            </div>
        </form>
    </div>
    </div>
</div>

@stop

@section('js')

@endsection