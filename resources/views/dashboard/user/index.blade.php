@extends('adminlte::page')

@section('css')
@stop

@section('content_header')

@stop

@section('content')
@include('dashboard.modal.deletemodal')

@if (session('success'))
<div class="alert alert-success">
    {{ session('success') }}
</div>
@endif
<div class="row pb-4">
    <div class="col-sm-12">
        @if($users->isNotEmpty())
        <div class="card">

            <!-- /.card-header -->

            <div class="card-body table-responsive">
                <table id="userstable" class="table table-hover text-nowrap">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $key=>$user)
                        <tr>
                            <td>{{ $users->firstItem() + $key }}</td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td><a href="{{route('users.edit', $user->id)}}" class="btn btn-transparent py-0"><i class="fas fa-edit text-dark"></i></a></td>
                            <td><button class="btn btn-transparent py-0" href="#deleteModalCenter" onclick="deleteData('<?php echo route('users.destroy', $user->id) ?>')" data-toggle="modal"><i class="fas fa-trash text-dark"></i></button></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{$users->links()}}
            </div>
            <!-- /.card-body -->
        </div>
        @else
        <div class="text-center">
            There's no Customer.
        </div>
        @endif
    </div>



    @stop

    @section('js')
    <script>
        function deleteData(route) {
            $(".deleteForm").attr('action', route);
            $("#deleteModalCenter").toggle();
        }

        $(function() {

            $('#userstable').DataTable({
                responsive: true,
                searching: false,
                info: false,
                paging: false,
                "aaSorting": [],
                "columnDefs": [

                    {
                        "orderable": false,
                        "targets": 3
                    },
                    {
                        "orderable": false,
                        "targets": 4
                    },
                ]
            });
        });
    </script>

    @endsection