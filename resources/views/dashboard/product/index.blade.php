@extends('adminlte::page')

@section('css')
@stop

@section('content_header')

@stop

@section('content')
@include('dashboard.modal.deletemodal')

@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif
<div class="row pb-4">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header py-4">
                <div class="card-tools mr-0">
                    <a href="{{route('products.create')}}" class="btn btn-dark">Create New Product</a>
                </div>
                <h3 class="font-weight-light">products</h3>
            </div>
            <!-- /.card-header -->
            @if($products->isNotEmpty())
            <div class="card-body table-responsive">
                <table id="productstable" class="table table-hover text-nowrap">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Name</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($products as $key=>$product)
                        <tr>
                            <th>{{ $products->firstItem() + $key }}</th>
                            <td> <a href="{{route('products.show', $product->slug)}}">{{$product->name}}</a> </td>
                            <td><a href="{{route('products.edit', $product->slug)}}" class="btn btn-transparent py-0"><i class="fas fa-edit text-dark"></i></a></td>
                            <td><button class="btn btn-transparent py-0" href="#deleteModalCenter" onclick="deleteData('<?php echo route('products.destroy', $product->slug) ?>')" data-toggle="modal"><i class="fas fa-trash text-dark"></i></button></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $products->links() }}
            </div>
            @else
            <div class="text-center">
                There's no Product.
            </div>
            @endif
            <!-- /.card-body -->
        </div>
    </div>

    

    @stop

    @section('js')
    <script>
        function deleteData(route) {
            $(".deleteForm").attr('action', route);
            $("#deleteModalCenter").toggle();
            }        

        $(function() {
            
            $('#productstable').DataTable({
                responsive: true,
                searching: false,
                info: false,
                paging: false,
                "aaSorting": [],
                "columnDefs": [

                    {
                        "orderable": false,
                        "targets": 3
                    },
                    {
                        "orderable": false,
                        "targets": 4
                    },
                ]
            });
        });
    </script>

    @endsection