@extends('adminlte::page')

@section('css')
<link rel="stylesheet" href="{{asset('css/trix.css')}}">
<style>
  trix-toolbar .trix-button-row .trix-button-group--file-tools {
    display: none;
  }
</style>
@stop

@section('content_header')

@stop

@section('content')

<div class="row pb-4">
  <div class="mt-3 col-12 offset-sm-2 col-sm-8 offset-lg-2 col-lg-8">
    <div class="card card-dark">
      <div class="card-header">
        <h3 class="card-title font-weight-light">Add New Product</h3>
      </div>

      <form role="form" method="POST" action="{{route('products.store')}}">
        @csrf
        <div class="card-body pb-1">

          
          <!-- name -->
          <div class="form-group">
            <label class="font-weight-light" for="name">name </label><span class="text-danger ml-1">*</span>
            <input type="text" class="form-control" id="name" name="name" value="{{old('name')}}" placeholder="Enter Product Name">
            @if ($errors->has('name'))
            <span class="help-block text-danger">
              <strong>{{ $errors->first('name') }}</strong>
            </span>
            @endif
          </div>

          <!-- price -->
          <div class="form-group">
            <label class="font-weight-light" for="price">Price </label><span class="text-danger ml-1">*</span>
            <input type="text" class="form-control" id="price" name="price" value="{{old('price')}}" placeholder="Enter Price">
            @if ($errors->has('price'))
            <span class="help-block text-danger">
              <strong>{{ $errors->first('price') }}</strong>
            </span>
            @endif
          </div>

          <!-- description -->
          <div class="form-group">
            <label class="font-weight-light" for="description">description</label>
            <input id="y" id="description" type="hidden" value="{{old('description')}}" name="description">
            <trix-editor input="y"></trix-editor>
            @if ($errors->has('description'))
            <span class="help-block text-danger">
              <strong>{{ $errors->first('description') }}</strong>
            </span>
            @endif
          </div>

          <!-- quantity -->
          <div class="form-group">
            <label class="font-weight-light" for="quantity">Stock Count </label><span class="text-danger ml-1">*</span>
            <input type="number" min="0" class="form-control" id="quantity" name="quantity" value="{{old('quantity')}}" placeholder="Enter Stock Quantity">
            @if ($errors->has('quantity'))
            <span class="help-block text-danger">
              <strong>{{ $errors->first('quantity') }}</strong>
            </span>
            @endif
          </div>

          <div class="form-group mt-5 text-center">
            <button type="submit" class="btn btn-primary mr-2">Create Product</button>
            <a class="btn btn-outline-primary" href="{{route('products.index')}}">Cancel</a>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

@stop

@section('js')
<script src="{{asset('js/trix.js')}}"></script>
@endsection