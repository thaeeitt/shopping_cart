@extends('adminlte::page')

@section('css')
@stop

@section('content_header')

@stop

@section('content')

<div class="row pb-4">
    <div class="offset-md-2 col-md-8 mt-3">
        <div class="card card-dark">
            <div class="card-header">
                <h3 class="card-title font-weight-light">Product Details</h3>
            </div>

            <div class="card-body">

                <div class="form-group mb-4">
                    <h5>{{$product->name}}</h5>
                </div>

                <div class="form-group mb-4 row">
                    <label class="font-weight-light col-sm-5">Stock Count</label>
                    <div class="col-sm-7">{{ $product->quantity }}</div>
                </div>

                <div class="form-group mb-4 row">
                    <label class="font-weight-light col-sm-5">Price</label>
                    <div class="col-sm-7">{{ number_format($product->price) }} MMK</div>
                </div>

                <div class="form-group mb-4 row">
                    <label class="font-weight-light col-sm-5">description</label>
                    <div class="col-sm-7">{!! $product->description !!}</div>
                </div>
                
            </div>

            <div class="card-footer bg-transparent border-top text-center">
                <a href="{{ route('products.index') }}" class="btn btn-dark">Back to Products List</a>
            </div>
        </div>
    </div>
</div>

@stop
