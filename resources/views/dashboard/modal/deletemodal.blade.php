 <!-- Model Box for delete -->
 <div class="modal modal-danger fade" id="deleteModalCenter" tabindex="-1" role="dialog" aria-labelledby="deleteModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header border-0 pb-0">
                <h1 class="modal-title text-primary flex-grow-1 text-center" id="exampleModalLongTitle">
                    <i class="fas fa-exclamation-circle"></i>
                </h1>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body text-center">
                <h3 class="font-weight-bold text-primary mb-3">Are you sure?</h3>
                <h5 class="font-weight-light">You are about this data record.</h5>
            </div>

            <div class="modal-footer mt-4">
                <form action="" method="post" class="deleteForm">
                    {{ csrf_field() }}
                    {{method_field('DELETE')}}
                    <button type="submit" class="btn btn-primary mr-2">Yes, Delete</button>
                    <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Cancel</button>
                </form>
            </div>
        </div>
    </div>
</div>