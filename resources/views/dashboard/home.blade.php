@extends('adminlte::page')

@section('css')
@stop

@section('content_header')

@stop

@section('content')

@if (session('success'))
<div class="alert alert-primary">
    {{ session('success') }}
</div>
@endif

<div class="row pt-3">
    
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-12">
        <a href="{{route('orders.index')}}">
        <div class="info-box">
            <span class="info-box-icon bg-danger"><i class="fa fa-shopping-cart"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Today Orders</span>
                <span class="info-box-number">{{$count}}</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        </a>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
</div>



@stop