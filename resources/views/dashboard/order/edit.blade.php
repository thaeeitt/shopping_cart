@extends('adminlte::page')

@section('css')
<link rel="stylesheet" href="{{asset('css/trix.css')}}">
<style>
    trix-toolbar .trix-button-row .trix-button-group--file-tools {
        display: none;
    }
</style>
@stop

@section('content_header')

@stop

@section('content')

<div class="row pb-4">
    <div class="mt-3 col-12 offset-sm-2 col-sm-8 offset-lg-2 col-lg-8">
        <div class="card card-dark">
            <div class="card-header">
                <h3 class="card-title font-weight-light">Edit Order</h3>
            </div>

            <form role="form" method="POST" action="{{route('orders.update', $order->order_no)}}" enctype="multipart/form-data">
                @method('put')
                @csrf
                <div class="card-body pb-1">

                    <!-- f name -->
                    <div class="form-group">
                        <label class="font-weight-light" for="first_name">First Name </label><span class="text-danger ml-1">*</span>
                        <input type="text" class="form-control" id="first_name" name="first_name" value="{{$order->first_name}}" placeholder="Enter order first_Name">
                        @if ($errors->has('first_name'))
                        <span class="help-block text-danger">
                            <strong>{{ $errors->first('first_name') }}</strong>
                        </span>
                        @endif
                    </div>

                    <!-- email -->
                    <div class="form-group">
                        <label class="font-weight-light" for="email">email </label><span class="text-danger ml-1">*</span>
                        <input type="email" class="form-control" id="email" name="email" value="{{$order->email}}" placeholder="Enter order email">
                        @if ($errors->has('email'))
                        <span class="help-block text-danger">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>

                    <!-- phone -->
                    <div class="form-group">
                        <label class="font-weight-light" for="phone">Phone </label><span class="text-danger ml-1">*</span>
                        <input type="text" class="form-control" id="phone" name="phone" value="{{$order->phone}}" placeholder="Enter order phone">
                        @if ($errors->has('phone'))
                        <span class="help-block text-danger">
                            <strong>{{ $errors->first('phone') }}</strong>
                        </span>
                        @endif
                    </div>

                    <!-- address -->
                    <div class="form-group">
                        <label class="font-weight-light" for="address">Address </label><span class="text-danger ml-1">*</span>
                        <input type="text" class="form-control" id="address" name="address" value="{{$order->address}}" placeholder="Enter order address">
                        @if ($errors->has('address'))
                        <span class="help-block text-danger">
                            <strong>{{ $errors->first('address') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group mt-5 text-center">
                        <button type="submit" class="btn btn-primary mr-2">Create order</button>
                        <a class="btn btn-outline-primary" href="{{route('orders.index')}}">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@stop

@section('js')
<script src="{{asset('js/trix.js')}}"></script>
@endsection