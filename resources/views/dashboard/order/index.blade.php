@extends('adminlte::page')

@section('css')
@stop

@section('content_header')

@stop

@section('content')
@include('dashboard.modal.deletemodal')

@if (session('success'))
<div class="alert alert-success">
    {{ session('success') }}
</div>
@endif
<div class="row pb-4">
    <div class="col-sm-12">
        @if($orders->isNotEmpty())
        <div class="card">
            <!-- /.card-header -->

            <div class="card-body table-responsive">
                <table id="orderstable" class="table table-hover text-nowrap">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Order No.</th>
                            <th>Customer Name</th>
                            <th>Phone</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($orders as $key=>$order)
                        <tr>
                            <td>{{ $orders->firstItem() + $key }}</td>
                            <td> <a href="{{route('orders.show', $order->order_no)}}">{{$order->order_no}}</a> </td>
                            <td>{{$order->user->name}}</td>
                            <td>{{$order->phone}}</td>
                            <td>{{date_format($order->created_at, 'd/M/Y')}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{$orders->links()}}
            </div>

            <!-- /.card-body -->
        </div>
        @else
        <div class="text-center">
            There's no Orders.
        </div>
        @endif
    </div>



    @stop

    @section('js')
    <script>
        function deleteData(route) {
            $(".deleteForm").attr('action', route);
            $("#deleteModalCenter").toggle();
        }

        $(function() {

            $('#orderstable').DataTable({
                responsive: true,
                searching: false,
                info: false,
                paging: false,
                "aaSorting": [],
                "columnDefs": [{
                        "orderable": false,
                        "targets": 5
                    },
                    {
                        "orderable": false,
                        "targets": 7
                    },
                    {
                        "orderable": false,
                        "targets": 6
                    },
                ]
            });
        });
    </script>

    @endsection