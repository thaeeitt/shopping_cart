@extends('adminlte::page')

@section('css')
@stop

@section('content_header')

@stop

@section('content')

<div class="row pb-4">
    <div class="col-md-5 col-sm-12 mt-3">
        <div class="card card-dark">
            <div class="card-header">
                <h3 class="card-title font-weight-light">Order Details</h3>
            </div>

            <div class="card-body">

                <div class="form-group mb-4">
                    <h5>{{$order->order_no}}</h5>
                </div>

                <div class="form-group mb-4 row">
                    <label class="font-weight-light col-sm-5">Customer Name</label>
                    <div class="col-sm-7">{{ $order->user->name}}</div>
                </div>

                <div class="form-group mb-4 row">
                    <label class="font-weight-light col-sm-5">Eamil</label>
                    <div class="col-sm-7">{{ $order->user->email }}</div>
                </div>

                <div class="form-group mb-4 row">
                    <label class="font-weight-light col-sm-5">Phone</label>
                    <div class="col-sm-7">{{ $order->phone }}</div>
                </div>

                <div class="form-group mb-4 row">
                    <label class="font-weight-light col-sm-5">Address</label>
                    <div class="col-sm-7">{{ $order->address }}</div>
                </div>

            </div>

            <div class="card-footer bg-transparent border-top text-center">
                <a href="{{ route('orders.index') }}" class="btn btn-dark">Back to Orders List</a>
            </div>
        </div>
    </div>

    <div class="col-md-7 col-sm-12 mt-3">
        <div class="card card-dark">
            <div class="card-header">
                <h3 class="card-title font-weight-light">Order Products</h3>
            </div>

            <div class="card-body">
                <div class="form-group mb-4 row">
                    <div class="col-sm-1">No.</div>
                    <div class="col-sm-5">Name</div>
                    <div class="col-sm-2">Qty</div>
                    <div class="col-sm-2">Price</div>
                    <div class="col-sm-2">Total</div>
                </div>

                @foreach ($order->products as $key=>$product)
                <div class="form-group mb-4 row">
                    <div class="font-weight-light col-sm-1">{{$key+1}}</div>
                    <div class="font-weight-light col-sm-5">{{$product->name}}</div>
                    <div class="font-weight-light col-sm-2">{{$product->pivot->quantity}}</div>
                    <div class="font-weight-light col-sm-2">{{number_format($product->pivot->unit_price)}}</div>
                    <div class="font-weight-light col-sm-2">{{number_format($product->pivot->total_price)}}</div>
                </div>
                @endforeach
                <hr>
                <div class="form-group mb-4 row">
                    <div class="font-weight-light col-sm-10">SubTotal</div>
                    <div class="font-weight-light col-sm-2">{{number_format($order->total)}}</div>
                </div>
                <div class="form-group mb-4 row">
                    <div class="font-weight-light col-sm-10">Total</div>
                    <div class="font-weight-light col-sm-2">{{number_format($order->total)}}</div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop