@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
    <br/>
    <div class="subheader text-center">
        <h2>
            Latest products
        </h2>
        
    </div>
</div>

    <!-- Latest products -->
    <div class="row">
        @forelse($products as $product)
            <div class="col-sm-3">

                <product :product="{{$product}}"
                         productlink="{{route('product',$product->slug)}}"
                         productimagepath='{{asset("images/$product->image")}}'
                >
                </product>

            </div>
        @empty
            <h3>No products</h3>
        @endforelse
        {{$products->links()}}
    </div>

    <!-- Footer -->
    <br>

</div>
@endsection






