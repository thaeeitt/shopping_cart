@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <br />
        <div class="subheader text-center">

        </div>
    </div>

    <!-- Latest products -->
    <div class="row" v-if="cart.length">
        <div class="col-md-6">
            <cart-detail :cart="cart" :carttotal="cartTotal" :totalitems="totalItems"></cart-detail>
        </div>
        <div class="col-md-6">
            <checkout-payment v-show="authenticated" :user="user" :cart="cart"></checkout-payment>
            <auth-form v-show="!authenticated"></auth-form>
            
        </div>
    </div>
    <div class="row justify-content-center" v-else>
        <br/>
        <div class="subheader text-center">
            <h2>Shopping Cart is Empty.</h2>
            <a href="/" class="btn btn-sm btn-success" >Continue Shopping</a href="/">
        </div>
    </div>

    <!-- Footer -->
    <br>

</div>
@endsection