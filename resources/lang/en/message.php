<?php

return [
    'object' => [
        'create' => 'New :Object is successfully created.',
        'update' => ':Object is successfully updated.',
        'delete' => ':Object is successfully deleted.',
    ]
];